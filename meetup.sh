#This is a companion script to the git meetup
#It covers from basic to medium/complex git usage
#It's intended to illustrate how to work with git following the git workflow, 
#in a team with at least two workers

#NOTE: This script depends on the existence of a git repository, named poetRy created somewhere

#Contents:
# - Initial Config
# - Basics
#  + Concepts: branches, remote and local
#  + Commands: status, fetch, checkout, branch, log, commit, push
# - Understanding workspace, index, and repository with diff
#  + Concepts: workspace, index, staging, remote, revisions, refs, HEAD, hash
#  + Commands: diff, blame
# - Git workflow: simple branch
#  + Concepts: git workflow, merge, fast-forward
#  + Commands: merge
# - Git workflow: complex branch
#  + Concepts: conflict, merge strategies, abort, reset, rebase
#  + Commands: pull, rebase, reset
# - Dealing with changes
#  + Concepts: changing between working branches, revert, undoing changes
#  + Commandas: stash, revert


#INITIAL CONFIG
mkdir meetup
cd meetup
#NOTE: create your own "poetRy" repository
git clone git@bitbucket.org:eduardgoma/poetry.git
mv poetry adela
git clone git@bitbucket.org:eduardgoma/poetry.git
mv poetry bernarda

#go to user A
cd adela

#see status and prepare initial commit config
git status
git config user.name "Adela"
git config user.email "eduard.goma@kernel-analytics.com"

#create branch develop of git workflow
git checkout -b develop

#initial file
echo "Verde que te quiero verde.
Verde viento. Verdes ramas.
El barco sobre la mar
y el caballo en la montaña." > poema.txt

#add file to index/staging
git add poema.txt
#commit file to repository
git commit -m "Initial commit"
#create remote branch and push file to it
git push origin develop

#go to user B
cd ../bernarda
git config user.name "Bernarda"
git config user.email "eduard.goma@kernel-analytics.com"


#BASICS

#bring remote state of repository to local
git fetch
#see branches
git branch -a
#change to branch develop
git checkout develop
#develop was not a local branch, but as we only have one origin and there is only one branch called develop
#git checkout develop is equal to
#git checkout -b develop --track origin/develop

#check branches now, see commits. develop already has commit (from fetch)
git branch -a #list all branches
git log #see commits history
git show-branch #fancy/cryptical way to see branches

#create new local branch to refactor color
git checkout -b LRC_1_refactor_color
git status
#refactor
sed -i 's/[V|v]erde/turquesa/g' poema.txt
#see how diff works
git diff
git add poema.txt
#difference between workspace and index/staging
git diff #no diff, as work is in index/staging
git diff --cached

git commit -m "LRC-1 Refactor colors"

#UNDERSTANDING WORKSPACE, INDEX AND REPOSITORY WITH DIFF

#difference between workspace, index and repository
git diff #no diff, as work is commited
git diff --cached #no diff, as work is committed
git diff HEAD..develop
git diff develop..HEAD
git diff develop..LRC_1_refactor_color
#what is HEAD?
cat .git/HEAD
git checkout develop
cat .git/HEAD #HEAD ref has changed!
git diff HEAD..develop
git diff HEAD..LRC_1_refactor_color
#return to branch
git checkout LRC_1_refactor_color
#add a couple of commits, save work in remote
sed -i 's/turquesa/rosa/g' poema.txt
git status
git commit -a -m "LRC-1 Refactor color. Change to pink"
sed -i 's/rosa/naranja/g' poema.txt
git commit -a -m "LRC-1 Refactor color. Change to orange"
git branch -a
git push origin LRC_1_refactor_color
#see new ref to remote branch
git branch -a
#play with revisions
git diff HEAD^..HEAD
git diff HEAD^^..HEAD
git diff HEAD^^^..HEAD
git diff HEAD~3..HEAD
git diff HEAD~3..develop
git diff HEAD~3..HEAD -- .
git diff HEAD~3..HEAD -- ./poema.txt
#use last hash (NOTE: replace with your own hash copied from git log output)
git diff b3eb2ef922~3..HEAD
git diff b3eb2ef9228bfd8e56d5665caa7663d52d407950~3..HEAD
#see file commit history
git blame ./poema.txt


#GIT WORKFLOW: local merge

#now, go and add a little more code from user A
cd ../adela
#see where we are, what we have in the workspace
git status
#add more code
echo "Con la sombra en la cintura
ella sueña en su baranda,
verde carne, pelo verde,
con ojos de fría plata" >> poema.txt
git status
git add poema.txt
git commit -m "LRC-2 Add personalization"
git status
#save to remote
git push origin develop
#create branch to change color and provoke conflict
git checkout -b LRC_change_color
sed -i 's/[V|v]erde/lila/g' poema.txt
git add poema.txt
git commit -m "LRC-3 Change color"
#change to develop again and see how workspace changed (i.e, change of colors)
git checkout develop
cat poema.txt #still green

#merge branch and save remote
git merge LRC_change_color
git push origin develop

#GIT WORKFLOW: merge with conflicts

#go back to user B
cd ../bernarda
git status
#as a good practice, do regularly pull from develop on branches
#4 ways to do so: fetch+merge, pull, fetch + rebase, pull+rebase

#create a copy to do some tries
cd ..
cp -r bernarda bernarda_merges
cd bernarda_merges

#OPTION 1: fetch + merge (from origin)
git status
git diff HEAD..origin/develop #see differences
git fetch
#go to develop and update branch with origin data
git checkout develop
git pull origin develop
#go back to LRC_1_refactor_color
git checkout LRC_1_refactor_color
git diff HEAD..develop #see differences now
git merge develop
#conflict achieved, see what's going on
git status #when merge is in progress, all files affected by merge are in index/staging
cat poema.txt
#if panic, abort. let's abort to try other strategies
git merge --abort
git status
cat poema.txt #abort achieved
#simple resolve strategy (which doesn't work)
git merge develop -s resolve
git merge --abort
#recursive strategy (which doesn't work)
git merge develop -s recursive
git merge --abort
#recursive strategy with options (it does work)
#we are TOTALLY sure our changes are OK
git merge develop -s recursive -Xours --no-commit
cat poema.txt
git merge --abort #abort to continue with meetup
#recursive strategy with options (it does work)
#we are TOTALLY sure incoming changes are the good ones
git merge develop -s recursive -Xtheirs --no-commit
cat poema.txt
git merge --abort #abort to continue with meetup

#OPTION 2: directly pull (which internaly is fetch + merge)
git pull origin develop
#another conflict. we can fix it and then commit, but let's abort
git merge --abort
#another way of aborting
git pull origin develop
git reset --merge

#OPTION 3: fetch changes, then rebase incoming commits with ours
git fetch
git rebase origin/develop
git rebase --abort

#OPTION 4: pull and rebase at the same time
git pull origin develop --rebase
git rebase --abort
#resetting the merge doesn't work in here
git pull origin develop --rebase
git reset --merge
git status #see messages, rebase in progress
git rebase --abort

#now that all 4 ways are tested, let's manually solve conflict in user B
cd ..
rm -rf bernarda_merges
cd bernarda

git status
git fetch
git merge origin/develop
#edit poema.txt and resolve conflicts manually
git add poema.txt
git commit -m "Manually merge. Override color change, orange is the good one"
#upload changes to remote
git push origin LRC_1_refactor_color

# Back to develop
git checkout develop
# Check for changes in develop
git fetch
git status
# Update local develop branch
git pull origin develop
#UNDOING CHANGES


########### Managing the Index: ADD, RESET
# ADDING MANY FILES (but not all)
git checkout -b LRC_add_multiple_poems
echo "Cuando cuento las semillas
sembradas alla abajo
para florecer así, lado a lado;

cuando examino a la gente
que tan bajo yace
para llegar tan alto;

cuando creo que el jardín 
que no verán los mortales
siega el azar sus capullos
y sortea a esta abeja,
puedo prescindir del verano, sin queja.

Autor del poema: Emily Dickinson" > dickinson.txt

echo "Mientras el cuerpo nos protege
del desastre

y un turbión hace cauce en nuestras venas
y se nos cubren los ojos de raíces agrias

mi alma sabe que allá del otro lado
en la esquina o tienda o consultorio
también tú te sufres en la oscuridad
con los brazos abiertos
para recibirme.

Autor del poema: Rogelio Guedea" > guedea.txt

echo "Cuando, bajo el montón cuadrangular
de tierra fresca que me ha de enterrar,

y después de ya mucho haber llovido,
cuando la hierba avance hacia el olvido,

aún, amigo, mi mirar de antaño,
cruzando el mar vendrá, sin un engaño,

a envolverte en un gesto enternecido,
como el de un pobre perro agradecido.

Autor del poema: Camilo Pessanha" > pesanha.txt

git status # We have 3 untracked files

git add dickinson.txt guedea.txt pesanha.txt # Add all files to be commited
git status # Index has 3 files that will be commited

git reset --mixed HEAD # Removes all files from Index
git status

git add . # Add all files to be commited. Shorter version when there are many files
git status
git commit -m "Add three new poems"

git status
git push origin LRC_add_multiple_poems

# Now we want to add the title to these new poems
echo -e "En un retrato\n$(cat pesanha.txt)" > pesanha.txt
echo -e "Del silencio\n$(cat guedea.txt)" > guedea.txt
echo -e "Cuando cuento las semillas\n$(cat dickinson.txt)" > dickinson.txt

git status
git diff dickinson.txt

# Remove all changes from local. Changes are lost!!
git reset --hard HEAD
git status


# Now we want to add the title + blank line to these new poems
echo -e "En un retrato\n\n$(cat pesanha.txt)" > pesanha.txt
echo -e "Del silencio\n\n$(cat guedea.txt)" > guedea.txt
echo -e "Cuando cuento las semillas\n\n$(cat dickinson.txt)" > dickinson.txt

git diff pesanha.txt

echo "Poco a poco

Poco a poco Dios nos quita la belleza humana:
poco a poco el árbol joven se marchita.
Ve y recita: 'Todo cuanto está dotado de vida,
acabará pereciendo'.
No te enamores de los huesos,
busca el espíritu.

Autor del poema: Yalal Al-Din Rumi" > aldinrumi.txt

git status

git add -u # Add all modified and deleted files (NO NEW FILES)
git status

git reset --mixed HEAD
git status

git add . # Add all new and modified files (NO DELETED)
git status

git reset pesanha.txt
git status

git mv pesanha.txt pessanha.txt
git status

git add pessanha.txt
git status

git commit -m "Add poem titles and aldinrumi poem. Rename pesanha.txt to pessanha.txt"
git push origin LRC_add_multiple_poems


######## Saving workspace changes
# Add new poem
echo "RELOJ PÚBLICO

De la torre mojada
el vetusto reloj
deja caer las horas como lágrimas.

Autor del poema: Francisco Monterde" > monterde.txt
cat monterde.txt

git status
git add monterde.txt

cat poema.txt
sed -i 's/lila/celeste/g' poema.txt

cat poema.txt
git status

git stash # Save current modifications in stash
git status # Working directory is clean

git stash list
git stash apply

git status # monterde.txt is in index to be added. poema.txt is modified but not in index

git stash show stash@{0}

git add poema.txt
git status

git stash
git status

git stash list

git stash show stash@{0}
git stash show stash@{1}

git stash apply

git status # poema.txt is not in staged in the index
git stash

git stash list
git stash apply stash@{1} --index # (In this stash we had poema.txt staged in the index)
git status # Now poema.txt is in Index because we have applied the stash with --index

# Remove changes to workspace
git reset --hard HEAD 
git status

git stash apply stash@{1} --index # (In this stash we had poema.txt staged in the index)

git reset --hard HEAD 
git status
git stash apply stash@{0} --index # (In this stash we had poema.txt IS NOT staged in the index)

git reset --hard HEAD
git status

git checkout develop

echo "Index of poems:
  
  Lorca: Romance sonámbulo
  " > index.txt
  
git status

git add index.txt # Add index.txt to staging area (Index)
git status
git commit -m "Add index."
git push origin develop

# Let's go back to branch where we added several poems
git checkout LRC_add_multiple_poems
git status

# Update branch with changes in develop
git pull origin develop
ls

# Get modifications from stash
git stash apply

# Push monterde.txt to the origin
git commit -m "Add monterde.txt poem"
git push origin LRC_add_multiple_poems

git status # There are still modifications to poema.txt (Change of color)

git stash # Stash changes before switching to a new branch
git checkout develop

git status
git fetch
# Get new poems from new branch
git pull origin LRC_add_multiple_poems
git status

git stash apply # Get modifications to poema.txt from stash
git diff poema.txt # Check changes

git checkout poema.txt # Remove modifications and get version from HEAD
git status
git stash apply # Recover modifications to poema.txt from stash
git diff poema.txt # Check changes

git add .
git status
git commit -m "Change color to celeste"
git push origin develop

############ UNDOING COMMITED CHANGES ##############
# Create a new branch to make crazy changes
git checkout -b LRC_10_make_crazy_changes

echo "This is crazy" >> poema.txt
git status

git add poema.txt
git status

git commit -m "LRC_10 Add a crazy comment"

git push origin LRC_10_make_crazy_changes

# Now we want to remove this commit because we realized it is crazy
# Options to try: --no-edit | -n/--no-commit
git log --oneline
git revert HEAD
git log --oneline
